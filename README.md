WebDAV docker container
=========================
Docker image with WebDAV configured using lighttpd. Exports /site/input volume,
so you can use that to provide remotely accessible directory for other container.

Quickstart:
-----------
	docker run -d -p 80:80 --name my_webdav mprasil/webdav

Customizing:
------------
This will run webdav server with username/password "user" so not very secure. 
You can provide your own htpasswd file:

	docker run -d -p 80:80 -v /path/to/htpasswd:/site/htpasswd:ro  --name my_webdav mprasil/webdav

You can obviously attach local directory to be shared/accessible via webdav

	docker run -d -p 80:80 -v /local/dir/:/site/input/ -v /path/to/htpasswd:/site/htpasswd:ro  --name my_webdav mprasil/webdav

Building your own:
------------------

In the dir with Dockerfile:

	docker build -t mywebdav .

